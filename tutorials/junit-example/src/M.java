
public class M {
	
	private Server server;
	
	public M(Server server) {
		this.server = server;
	}

	public int zero() {
		return 0;
	}
	
	public void failIfZero(int input) {
		if (input == 0) {
//			throw new IllegalArgumentException("Zero!!!");
		}
	}
	
	public String get_data() {
		String data = server.get_data();
		return data;
	}

}
